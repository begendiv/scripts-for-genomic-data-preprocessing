# Scripts for Genomic Data Preprocessing

This repository contains scripts for genomic data preprocessing.

## 1. demultiplexFromHeader_v2.py

Goal of the script: Demultiplex raw reads using degenerated indexes

Running the script:

`python demultiplexFromHeader_v2.py -fq1 Undetermined_R1.fastq.gz -fq2 Undetermined_R2.fastq.gz -ss sampleSheet.txt -out ./outPath/`

Author: Maximilian Driller