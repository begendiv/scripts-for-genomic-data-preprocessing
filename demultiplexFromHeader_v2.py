import argparse, gzip, threading, os, subprocess

# generates all 
def generateDIST1(word):
	perturbations=[]
	for i, letter in enumerate(word):
		if letter=="A":
			for l in ["C", "G", "T", "N"]:
				pertub=word[:i] + l + word[i+1:]
				perturbations.append(pertub)
				#print(pertub)
		elif letter=="C":
			for l in ["A", "G", "T", "N"]:
				pertub=word[:i] + l + word[i+1:]
				perturbations.append(pertub)
				#print(pertub)
		elif letter=="G":
			for l in ["A", "C", "T", "N"]:
				pertub=word[:i] + l + word[i+1:]
				perturbations.append(pertub)
				#print(pertub)
		elif letter=="T":
			for l in ["A", "C", "G", "N"]:
				pertub=word[:i] + l + word[i+1:]
				perturbations.append(pertub)
				#print(pertub)

	return(perturbations)

parser = argparse.ArgumentParser(description="demultiplexes samples into pools using the p7 index (just Ns as a barcodes do NOT word yet!)")
parser.add_argument("-fq1", "--fastq1", help="path to fastq file R1 (can be zipped)", type=str, required=True)
parser.add_argument("-fq2", "--fastq2", help="path to fastq file R2 (can be zipped)", type=str, required=True)
parser.add_argument("-ss", "--sampleSheet", help="path to sampleSheet; 3 columns: sampleID, p7, p5 (tab seperated)", type=str, required=True)
parser.add_argument("-out", "--outDir", help="outPut directory where demultiplxed samples will be written", type=str, required=True)


args = parser.parse_args()

subprocess.call("mkdir -p %s"%(args.outDir), shell=True)
subprocess.call("rm -r %s/*"%(args.outDir), shell=True)


sampleParser=open(args.sampleSheet, "rt")

if args.fastq1.endswith(".gz"):
	fqParser1=gzip.open(args.fastq1, "rt")
else:
	fqParser1=open(args.fastq1, "rt")

if args.fastq2.endswith(".gz"):
	fqParser2=gzip.open(args.fastq2, "rt")
else:
	fqParser2=open(args.fastq2, "rt")


pertubHash={}
sampleHash={}

for line in sampleParser:
	sID,p7,p5=line.rstrip().split("\t")

	p7_alts=generateDIST1(p7)
	#print(p7_alts)
	#print(len(p7_alts))

	for p7_alt in p7_alts:
		pertubHash[p7_alt]=p7

	pertubHash[p7]=p7
	sampleHash[p7]=sID



lineCount=0
seqCount=0
write=False
for line in fqParser1:
	line2=fqParser2.readline()
	lineCount+=1
	if lineCount==1:
		barcodes=line.rstrip().split(" ")[-1].split(":")[-1]
		p7,p5=barcodes.split("+")

		if p7 in pertubHash:
			sampleP7=pertubHash[p7]
			sampleID=sampleHash[sampleP7]
			
			write=True
			oWriter=gzip.open(args.outDir + "/" + sampleID + "_R1.fastq.gz", "at")
			oWriter2=gzip.open(args.outDir + "/" + sampleID + "_R2.fastq.gz", "at")
			oWriter.write(line)
			oWriter2.write(line2)

	elif lineCount==2 or lineCount==3:
		if write==True:
			oWriter.write(line)
			oWriter2.write(line2)
	elif lineCount==4:
		if write==True:
			oWriter.write(line)
			oWriter2.write(line2)
			oWriter.close()
			oWriter2.close()
		write=False
		lineCount=0
		seqCount+=1
fqParser1.close()
fqParser2.close()
